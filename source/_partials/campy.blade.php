<div class="mt-4 mb-4 p-4 bg-purple-lightest rounded-lg">

    <p class="text-4xl mb-4 font-bold text-purple">Anmeldung</p>

    <a href="https://app.code.design/register/{{ $camp_id }}" class="bg-purple text-white no-underline inline-block text-3xl font-bold px-4 py-2 leading-none rounded-lg hover:border-transparent hover:bg-purple-dark mt-4">
        Anmelden
    </a>

    <div class="mt-6 ml-1">
        <p>Deine Anmeldung ist verbindlich.</p>
        <p>Du musst uns dann noch ein paar Infos zu dir schicken und ggf. bezahlen.</p>
        <p>Die Anleitung dazu bekommst du per E-Mail.</p>
    </div>
</div>
