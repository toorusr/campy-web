@extends('_layouts.master')

@section('body')
<div class="p-4 bg-purple">
    <div class="bg-white p-4">
        <p class="text-2xl mb-4">News</p>
        <p>In nächster Zeit werden hier News und Updates rund ums Programmieren kommen.</p>
    </div>
</div>
@endsection

@section('title')
News
@endsection
