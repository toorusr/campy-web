<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <title>Code+Design | @yield('title')</title>

        <!-- Start: Favicons -->
        {{-- <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png"> --}}
        <link rel="icon" type="image/png" href="/img/favicon-128x128.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/img/favicon-128x128.png" sizes="16x16">
        <!-- End: Favicons -->

        <!-- Start: Open Graph -->
        <meta property="og:title" content="@yield('og:title')" /> <!-- Same title as in line 5 ? -->
        <meta property="og:description" content="@yield('og:description')" /> <!-- Same description as in line 14 ? -->
        <meta property="og:url" content="@yield('og:url')" />
        <meta property="og:image" content="@yield('og:image')" />
        <!-- End: Open Graph -->

        <!-- Start: Twitter Card -->
        <meta name="twitter:card" content="@yield('twitter:card')">
        <meta name="twitter:site" content="@yield('twitter:site')">
        <meta name="twitter:creator" content="@yield('twitter:creator')">
        <meta name="twitter:title" content="@yield('twitter:title')"> <!-- Same title as in line 5 ? -->
        <meta name="twitter:description" content="@yield('twitter:description')"> <!-- Same description as in line 14 ? -->
        <meta name="twitter:image" content="@yield('twitter:image')">
        <!-- End: Twitter Card -->

        <!-- Start: Other meta -->
        <meta name="description" content="@yield('description')">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <!-- End: Other meta -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">

        <!-- Start: Styles -->
        <style>
            @yield('styles');
        </style>
        <!-- End: Styles -->
        <!-- Start: Scripts -->
        <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
        <script type="text/javascript">
	       FreshWidget.init("", {"queryString": "&widgetType=popup&submitTitle=Abschicken&captcha=yes&searchArea=no", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Support", "buttonColor": "white", "buttonBg": "#000924", "alignment": "2", "offset": "235px", "formHeight": "\"500\"px", "captcha": "yes", "url": "https://codedesign.freshdesk.com"} );
        </script>
        <!-- End: Scripts -->
    </head>
    <body class="font-sans antialiased">

        @include('_partials.navbar')
        <main>
            @yield('body')
        </main>
        @include('_partials.footer')

        <!-- Start: Scripts -->
        <script src="https://cdn.jsdelivr.net/npm/lazysizes@4.0.1/lazysizes.min.js"></script>
        @include('_partials.scripts')
        @yield('scripts')
        <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
        <!-- End: Scripts -->

    </body>
</html>
